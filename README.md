# Mac M1に jupyter-notebook を設定

* 参考　https://degitalization.hatenablog.jp/entry/2020/12/07/154628

## Python - Jupyter NotebookをMac M1 搭載機にインストールする手順

### Pythonをインストール

```
brew install python
```

```
python3.10 -m pip install --upgrade pip
```

```
python3 --version
```

### Jupyter NotebookをM1 Macにインストール

* Miniconda を使用したインストール　と　Homebrewを使用したインストール　は失敗するみたい。

* [2021.02.08] --> Homebrewの正式サポートバージョンが発表されました。 https://degitalization.hatenablog.jp/entry/2021/04/07/190000 

#### Pip3を使用したインストール

```
sudo -H pip3 install jupyter
```

* Jupyter Notebookを起動

```
jupyter notebook
```


#### Pythonの画像処理ライブラリPillow(PIL)の使い方

* https://note.nkmk.me/python-pillow-basic/

* インストール

```
pip install Pillow
```

* 公式ドキュメントがしっかりしていて分かりやすい。

[Pillow — Pillow (PIL Fork) 6.1.0 documentation](https://pillow.readthedocs.io/en/stable/)


* 画像読み込み

```py
from PIL import Image, ImageFilter

im = Image.open('data/src/lenna_square.png')
```

* フォーマット、サイズ（幅、高さ）、モードなどのメタ情報を取得。

```py
print(im.format, im.size, im.mode)
# PNG (512, 512) RGB
```

* RGB各色の最小値と最大値を取得。

```py
print(im.getextrema()) 
# ((54, 255), (3, 248), (8, 225))
```

* 画像を処理する例として、白黒変換（convert('L')）、90度回転（rotate(90)）、ガウシアンブラー（filter()）を行う。

```py
new_im = im.convert('L').rotate(90).filter(ImageFilter.GaussianBlur())
```

* 画像を保存。

```py
new_im.save('data/dst/lenna_square_pillow.jpg', quality=95)
```

* 図形描画

```py
from PIL import Image, ImageDraw, ImageFont

im = Image.new("RGB", (512, 512), (128, 128, 128))
```

```py
draw = ImageDraw.Draw(im)
```

#### NumPyのインストール

```
pip install numpy
```

#### PythonでOpenCVを使う

```
pip install opencv-python
```

or

```
brew install opencv
```

* Pythonのsite-packagesの場所を調べる

```
$ python
>>> import site; site.getsitepackages()
['/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages']
```

* OpenCVへのシンボリックリンクを生成する

```
cd /Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages
```

```
ln -s /usr/local/Cellar/opencv/3.4.1-4/lib/python3.6/site-packages/cv2.cpython-36m-darwin.so ./
```

* cv2が使えるか確認

```
$ python
>>> import cv2
>>> cv2.__version__
'3.4.1-4'
```

#### Matplotlibでグラフ描画が簡単に？pipのインストール手順を解説！

* https://and-engineer.com/articles/Yca7eRAAACMAqno2

```
pip install matplotlib
```

* MatplotlibをJupyter Notebookからインストールする

   * Jupyter Notebookを既に利用している方は「Notebook」のセルに、先頭に「！」マークをつけて「!pip install matplotlib」を実行してMatplotlibをインストールできます。pipと同様に「Successfully installed matplotlib-3.5.1」の表示が出ればインストール完了です。













